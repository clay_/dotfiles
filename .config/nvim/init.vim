set nocompatible nu
filetype off
syntax on
call plug#begin('~/.local/share/nvim/plugged')

Plug 'flazz/vim-colorschemes'
Plug 'scrooloose/nerdtree'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'rust-lang/rust.vim'

function! YCMInstall(info)
  if a:info.status == 'installed'
    !./install.sh --clang-completer
  endif
endfunction
Plug 'Valloric/YouCompleteMe', { 'on': [], 'do': function('YCMInstall') }
let g:ycm_confirm_extra_conf    = 0
let g:ycm_global_ycm_extra_conf = '~/.config/nvim/ycm.py'
let g:ycm_extra_conf_vim_data   = ['&filetype']
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_enable_diagnostic_signs = 0
augroup load_ycm
  autocmd!
  autocmd! InsertEnter *
        \ call plug#load('YouCompleteMe')     |
        \ if exists('g:loaded_youcompleteme') |
        \   call youcompleteme#Enable()       |
        \ endif                               |
        \ autocmd! load_ycm
augroup END

call plug#end()
filetype plugin indent on
let g:airline_powerline_fonts = 1
let g:airline_theme='bubblegum'

set nonumber norelativenumber

set t_Co=16
set background=dark
colorscheme Tomorrow-Night-Eighties

let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors

set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab